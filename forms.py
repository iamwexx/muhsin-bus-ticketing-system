from flask_wtf import Form

#Flask-WTF uses wtforms behind the scenes for the actual form, field, and widget creation.
from wtforms import PasswordField,DateField,DateTimeField
from wtforms import StringField,SelectField, BooleanField
from wtforms import TextAreaField,IntegerField, widgets, SelectMultipleField
from wtforms.validators import DataRequired, Optional
from wtforms.validators import Email
from wtforms.validators import EqualTo
from wtforms.validators import Length
from wtforms.validators import Regexp
from wtforms.validators import ValidationError


from models import Booking
# from models import Admin


# def id_exists(form, field):
#     """Tests a new id to see if it already exists in the database"""
#     if User.select().where(User.national_id== field.data).exists():
#         raise ValidationError('ID already exists.')

# def email_exists(form, field):
#     """Tests a new email to see if it already exists in the database"""
#     if User.select().where(User.email == field.data).exists():
#         raise ValidationError('User with that email already exists.')
def serial_exists(form, field):
    """Tests a  serial to see if it already exists in the database"""
    if Booking.select().where(Booking.serial_no == field.data).exists():
        pass
    else:
        raise ValidationError('Ticket Number Does NOT exists.')

def loc_not_equal(form, field):
    if field.data == form.location_from.data:
        raise ValidationError('Location Cannot Match')
l = ['1','2','3','4','5','6','7','8','9','10','11','12','13',
        '14','15','16','17','18','19','20','21','22','23','24','25',
        '26','27','28','29','30','31','32','33','34','35','36','37',
        '38','39','40','41','42','43','44','45','46','47','48','49','50']

DEFAULT_CHOICES = [(c, c) for c in l]
class BusSearch(Form):
  
       
    location_from = SelectField(
        'FROM',
          choices=[(c, c) for c in [
            'Bungoma','Busia','Eldoret','Emali','Embu','Garissa','Homa Bay','Isiolo','Kitui','Kajiado','Kakamega',
            'Kericho','Kiambu','Kilifi','Kisii','Kisumu','Kitale','Lamu','Langata','Lodwar','Machakos','Malindi',
            'Mandera','Marsabit','Meru','Mombasa','Moyale','Mumias','Muranga','Nairobi','Naivasha','Nakuru','Narok',
            'Nyahururu','Nyeri','Ruiru','Thika','Vihiga','Voi','Wajir','Watamu','Webuye'
            ]])

    location_to = SelectField(
        'TO',
          choices=[(c, c) for c in [
            'Bungoma','Busia','Eldoret','Emali','Embu','Garissa','Homa Bay','Isiolo','Kitui','Kajiado','Kakamega',
            'Kericho','Kiambu','Kilifi','Kisii','Kisumu','Kitale','Lamu','Langata','Lodwar','Machakos','Malindi',
            'Mandera','Marsabit','Meru','Mombasa','Moyale','Mumias','Muranga','Nairobi','Naivasha','Nakuru','Narok',
            'Nyahururu','Nyeri','Ruiru','Thika','Vihiga','Voi','Wajir','Watamu','Webuye'
            ]],
            validators = [
                DataRequired(),
                loc_not_equal])

    date_dep = DateField(
        'DEPARTURE DATE: DD-MM-YYYY',
        validators = [
            DataRequired(message = "Date required, Format: DD-MM-YYYY")],
        format='%d-%m-%Y')

    date_active = BooleanField(
        'NON-SPECIFIC DATE'
        )
class TripForm(Form):
    """Class for Trip creation form"""
    location_from = SelectField(
        'LOCATION OF DEPTURE',
        choices=[(c, c) for c in [
            'Bungoma','Busia','Eldoret','Emali','Embu','Garissa','Homa Bay','Isiolo','Kitui','Kajiado','Kakamega',
            'Kericho','Kiambu','Kilifi','Kisii','Kisumu','Kitale','Lamu','Langata','Lodwar','Machakos','Malindi',
            'Mandera','Marsabit','Meru','Mombasa','Moyale','Mumias','Muranga','Nairobi','Naivasha','Nakuru','Narok',
            'Nyahururu','Nyeri','Ruiru','Thika','Vihiga','Voi','Wajir','Watamu','Webuye'
            ]])
    
    location_to = SelectField(
        'LOCATION OF ARRIVAL',
        choices=[(c, c) for c in [
            'Bungoma','Busia','Eldoret','Emali','Embu','Garissa','Homa Bay','Isiolo','Kitui','Kajiado','Kakamega',
            'Kericho','Kiambu','Kilifi','Kisii','Kisumu','Kitale','Lamu','Langata','Lodwar','Machakos','Malindi',
            'Mandera','Marsabit','Meru','Mombasa','Moyale','Mumias','Muranga','Nairobi','Naivasha','Nakuru','Narok',
            'Nyahururu','Nyeri','Ruiru','Thika','Vihiga','Voi','Wajir','Watamu','Webuye'
            ]] ,
            validators = [
                DataRequired(),
                loc_not_equal])
    
   
    date_dep = DateField(
        'DEPARTURE DATE',
        validators = [
            DataRequired(message = "Date required, Format: DD-MM-YYYY")],
        format='%d-%m-%Y')

    time_dep = DateTimeField(
        'DEPARTURE TIME',
        validators = [
            DataRequired()],
        format='%H:%M')

    bus_no = StringField(
            'BUS NUMBER', # First argument is the label
            validators = [  # Second argument is an array of validators
                DataRequired(),     # Built in to wtf
                Regexp(     # Defines a regex pattern as a validator
                    r'^[0-9]+$',
                    message = ("Invalid number")

                ),
                Length(min=4, max=4, message=("Should be atlease four digits"))
                
            ])
    price = StringField(
            'PRICE: KSH-', # First argument is the label
            validators = [  # Second argument is an array of validators
                DataRequired(),     # Built in to wtf
                Regexp(     # Defines a regex pattern as a validator
                    r'^[0-9]+$',
                    message = ("Invalid number")
                )
                
            ])
    # timestamp = SelectField(
    #     'DEPARTURE TIME',
    #     choices=[(c, c) for c in ['4:00 AM','7:00 AM','9:00 AM','11:00 AM', '2:00 PM', '4:00 PM','8:00 PM', '11:00 PM']])

class BookingForm(Form):
    """Class for Regsitration form validation"""
    firstname = StringField(
        'First Name', # First argument is the label
        validators = [  # Second argument is an array of validators
            DataRequired(),     # Built in to wtf
            Regexp(     # Defines a regex pattern as a validator
                r'^[a-zA-Z]+$',
                message = ("First name should be one word and letters")
            )
            
        ])
    
    secondname = StringField(
        'Second Name', # First argument is the label
        validators = [  # Second argument is an array of validators
            DataRequired(),     # Built in to wtf
            Regexp(     # Defines a regex pattern as a validator
                r'^[a-zA-Z]+$',
                message = ("Second name should be one word and letters")
            )
           
        ])
    
    id_passpt = StringField(
        'ID/Passport', # First argument is the label
        validators = [  # Second argument is an array of validators
            DataRequired(),     # Built in to wtf
            Regexp(     # Defines a regex pattern as a validator
                r'^[A-Z0-9]+$',
                message = ("Invalid id/passport number")
            )
            
        ])
    

    email = StringField(
        'Email (Optional)',
        validators = [
            Optional(strip_whitespace=True),
            Email()
            
        ])
    nationality = SelectField(
        'Nationality',
        choices=[(c, c) for c in ['Kenyan','European','American','Chineese','South African','Nigerian','Indian','Sudanees']])

    phone_num = StringField(
        'Phone Number: +254*********', # First argument is the label
        validators = [  # Second argument is an array of validators
            DataRequired(),     # Built in to wtf
            Regexp(     # Defines a regex pattern as a validator
                r'^[\+254][0-9]+$',   
                message = ("Invalid number (+254---)")
            )
            
        ])


class LoginForm(Form):
    email = StringField(
        'Email',
        validators = [
            DataRequired(),
            Email()
        ])
    password = PasswordField(
        'Password',
        validators = [
            DataRequired()
        ])


class SeatForm1(Form):

    example = SelectMultipleField('Seates', choices=DEFAULT_CHOICES ,option_widget=widgets.CheckboxInput(),widget=widgets.ListWidget(prefix_label=False))


class TicketCheck(Form):
    """Check Ticket status"""
    ticket_no = StringField(
            'Ticket Number', # First argument is the label
            validators = [  # Second argument is an array of validators
                DataRequired(),     # Built in to wtf
                Regexp(     # Defines a regex pattern as a validator
                    r'^[0-9]+$',
                    message = ("Invalid number")
                ),
                serial_exists,
                Length(min=5, max=5, message=("Invalid number: Five digits required"))
                
            ])
    
        