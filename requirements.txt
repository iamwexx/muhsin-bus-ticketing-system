Flask==0.10.1
Flask-WTF==0.9.4
WTForms==1.0.5
peewee==2.8.1
Flask-Login==0.3.2
Flask-Bcrypt==0.7.1