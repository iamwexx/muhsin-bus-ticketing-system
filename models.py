""" Basic setup for a user database """

import datetime

from flask_bcrypt import generate_password_hash
from flask_bcrypt import check_password_hash
#Flask keeps external packages like flask-login under this ext extension
from flask_login import UserMixin

from peewee import *
import random

DATABASE = SqliteDatabase('muhsin_database.db')
def Serial_no_gen():
        new_serial_no = random.randint(10000,99999)

        if Booking.select().where(Booking.serial_no == new_serial_no).exists():
            Serial_no_gen()
        else:
            return new_serial_no


#Mixins are small functionality adders, so put them before the main inheritance class
class User(UserMixin, Model):
    
    email = CharField(unique=True)
    password = CharField(max_length=100)
    
    class Meta:
        database = DATABASE
        

    #Class method decorator to indicate a function that creates an instance of the class
    #This insures that we can do things like hash the password when we make a user
    #Circumvents the User.create() method (That wont hash the password)
    @classmethod
    def create_user(cls,email, password):
        """Secure user creation in the database"""
        
        try:
            with DATABASE.transaction():
                cls.create(
                    email = email,
                    password = generate_password_hash(password),
                    )
        except IntegrityError:
            pass

class Trip(Model):
    from_loc = CharField(unique=False)
    to_loc = CharField(unique=False)
    date_dep = DateField(unique=False)
    time_dep = TimeField(unique=False)
    bus_no = CharField(unique=False)
    price = CharField(unique=False)
    active = BooleanField(unique=False, default = False)
    

    @classmethod
    def create_trip(cls,from_loc,to_loc ,date_dep,time_dep, bus_no, price,active = True):
        """Secure user creation in the database"""
        
        try:
            with DATABASE.transaction():
                cls.create(
                    
                    from_loc = from_loc,
                    to_loc = to_loc,
                    date_dep = date_dep, 
                    time_dep = time_dep,
                    bus_no = bus_no,
                    price = price,
                    active = active)
        except IntegrityError:
            pass
   
    class Meta:
        database = DATABASE
        order_by = ('-date_dep','-time_dep',)
    
    # def get_taken_seats(self):
    #     """Get all seats taken in a given trip."""
    #     return Ticket.select().where(Ticket.trip == self).seat_no

    def get_travellers(self):
        """Get all traveller a given trip."""
        return Bookings.select().where(Bookings.trip== self)
               

class Booking(Model):
    trip = ForeignKeyField(rel_model=Trip, related_name='ticket')
    serial_no = CharField(unique=True)
    firstname = CharField(unique=False)
    secondname = CharField(unique=False)
    id_passpt = CharField(unique=False)
    nationality = CharField(unique=False)
    phone_num = CharField(unique=False)
    email = CharField(unique=False)
    seat_no = CharField(unique=False)
    paid_for = BooleanField(unique=False, default = False)

    @classmethod
    def make_booking(cls, trip, firstname, secondname, id_passpt, nationality, phone_num, email, seat_no, paid_for = False):
        """Secure user creation in the database"""
        
        try:
            with DATABASE.transaction():
                cls.create(
                    trip = trip,
                    serial_no = Serial_no_gen(),
                    firstname = firstname,
                    secondname = secondname,
                    id_passpt = id_passpt,
                    nationality = nationality,
                    phone_num = phone_num,
                    email = email,
                    seat_no = seat_no,
                    paid_for = paid_for)
        except IntegrityError:
            pass

   
    paid = 'paid_for'
    
    @classmethod
    def update_payment(cls, row_id):
        cls.update(**{cls.paid: True}).where(cls.id == row_id).execute()

    class Meta:
        database = DATABASE
        order_by = ('-id',)

class SeatConfig(Model):
    trip = ForeignKeyField(rel_model=Trip, related_name='seatConfig')
    S1 = BooleanField(unique=False, default = False)
    S2 = BooleanField(unique=False, default = False)
    S3 = BooleanField(unique=False, default = False)
    S4 = BooleanField(unique=False, default = False)
    S5 = BooleanField(unique=False, default = False)
    S6 = BooleanField(unique=False, default = False)
    S7 = BooleanField(unique=False, default = False)
    S8 = BooleanField(unique=False, default = False)
    S9 = BooleanField(unique=False, default = False)
    S10 = BooleanField(unique=False, default = False)
    S11 = BooleanField(unique=False, default = False)
    S12 = BooleanField(unique=False, default = False)
    S13 = BooleanField(unique=False, default = False)
    S14 = BooleanField(unique=False, default = False)
    S15 = BooleanField(unique=False, default = False)
    S16 = BooleanField(unique=False, default = False)
    S17 = BooleanField(unique=False, default = False)
    S18 = BooleanField(unique=False, default = False)
    S19 = BooleanField(unique=False, default = False)
    S20 = BooleanField(unique=False, default = False)
    S21 = BooleanField(unique=False, default = False)
    S22 = BooleanField(unique=False, default = False)
    S23 = BooleanField(unique=False, default = False)
    S24 = BooleanField(unique=False, default = False)
    S25 = BooleanField(unique=False, default = False)
    S26 = BooleanField(unique=False, default = False)
    S27 = BooleanField(unique=False, default = False)
    S28 = BooleanField(unique=False, default = False)
    S29 = BooleanField(unique=False, default = False)
    S30 = BooleanField(unique=False, default = False)
    S31 = BooleanField(unique=False, default = False)
    S32 = BooleanField(unique=False, default = False)
    S33 = BooleanField(unique=False, default = False)
    S34 = BooleanField(unique=False, default = False)
    S35 = BooleanField(unique=False, default = False)
    S36 = BooleanField(unique=False, default = False)
    S37 = BooleanField(unique=False, default = False)
    S38 = BooleanField(unique=False, default = False)
    S39 = BooleanField(unique=False, default = False)
    S40 = BooleanField(unique=False, default = False)
    S41 = BooleanField(unique=False, default = False)
    S42 = BooleanField(unique=False, default = False)
    S43 = BooleanField(unique=False, default = False)
    S44 = BooleanField(unique=False, default = False)
    S45 = BooleanField(unique=False, default = False)
    S46 = BooleanField(unique=False, default = False)
    S47 = BooleanField(unique=False, default = False)
    S48 = BooleanField(unique=False, default = False)
    S49 = BooleanField(unique=False, default = False)
    S50 = BooleanField(unique=False, default = False)


    @classmethod
    def make_seatconfig(cls, trip):
        """Secure user creation in the database"""
        
        try:
            with DATABASE.transaction():
                cls.create(
                    trip = trip,
                    S1=False,S2=False,S3=False,S4=False,S5=False,
                    S6=False,S7=False,S8=False,S9=False,S10=False,
                    S11=False,S12=False,S13=False,S14=False,S15=False,
                    S16=False,S17=False,S18=False,S19=False,S20=False,
                    S21=False,S22=False,S23=False,S24=False,S25=False,
                    S26=False,S27=False,S28=False,S29=False,S30=False,
                    S31=False,S32=False,S33=False,S34=False,S35=False,
                    S36=False,S37=False,S38=False,S39=False,S40=False,
                    S41=False,S42=False,S43=False,S44=False,S45=False,
                    S46=False,S47=False,S48=False,S49=False,S50=False)
        except IntegrityError:
            pass
    
    seats= ['S1','S2','S3','S4','S5','S6','S7','S8','S9','S10','S11',
            'S12','S13','S14','S15','S16','S17','S18','S19','S20','S21',
            'S22','S23','S24','S25','S26','S27','S28','S29','S30','S31',
            'S32','S33','S34','S35','S36','S37','S38','S39','S40','S41',
            'S42','S43','S44','S45','S46','S47','S48','S49','S50']

    @classmethod
    def update_seat(cls, trip_id, seat_no):
        cls.update(**{cls.seats[int(seat_no)-1]: True}).where(cls.trip == trip_id).execute()
    @classmethod 
    def get_taken_seats(cls,trip_id):
        ll = []
        # vv = []
        query = cls.get(cls.trip == trip_id)
        gg =[query.S1,query.S2,query.S3,query.S4,query.S5,query.S6,query.S7,query.S8,query.S9,query.S10,
            query.S11,query.S12,query.S13,query.S14,query.S15,query.S16,query.S17,query.S18,query.S19,query.S20,
            query.S21,query.S22,query.S23,query.S24,query.S25,query.S26,query.S27,query.S28,query.S29,query.S30,
            query.S31,query.S32,query.S33,query.S34,query.S35,query.S36,query.S37,query.S38,query.S39,query.S40,
            query.S41,query.S42,query.S43,query.S44,query.S45,query.S46,query.S47,query.S48,query.S49,query.S50]
        
        for x in range(1,51):
            if gg[x-1] == False:
                ll.append(str(x))

        return ll
        
    class Meta:
        database = DATABASE
        order_by = ('trip',)


def initialize():
    """Called when the program starts if not called as an imported module."""
    DATABASE.connect()
    DATABASE.create_tables([User, Trip, Booking, SeatConfig], safe=True)
    DATABASE.close()
