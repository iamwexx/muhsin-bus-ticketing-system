from flask import Flask

from flask import flash
#g is a global object we can attach things to for global access between views and modules
from flask import g, jsonify
from flask import redirect, request
from flask import render_template, make_response
from flask import url_for, abort
from flask_bcrypt import check_password_hash

from flask_login import current_user
#LoginManager - An appliance to handle user authentication.
from flask_login import LoginManager
#login_user - Function to log a user in and set the appropriate cookie so they'll be considered authenticated by Flask-Login
from flask_login import login_user
from flask_login import logout_user
from flask_login import login_required

import forms
import models
import webbrowser 

# from flask import make_response
# from reportlab.pdfgen import canvas

#server variables
DEBUG = True
PORT = 8080
HOST = '127.0.0.1'

app = Flask(__name__)
app.secret_key = 'nbUTWE9yhr/xDLosbxcInOLIabzaypvmdHyhITw5odIHmrduQeh2G'

#login manager setup
login_manager = LoginManager()  #creates instance of the manager
login_manager.init_app(app)     #setup of manager for our app (passing the app to the manager)
login_manager.login_view = 'login'      #name of the view we redirect people not logged in yet. Manager does this automatically

#user_loader - A decorator to mark the function responsible for loading a user from whatever data source we use.
@login_manager.user_loader
def load_user(userid):
    try:
        return models.User.get(models.User.id == userid)    #getting the User with an id equal to this Userid
    except models.DoesNotExist:     #This is a peewee exception
        return None

# before_request - A decorator to mark a function as running before the request hits a view.
@app.before_request
def before_request():
    """Connect to the database before each request"""
    g.db = models.DATABASE
    g.db.connect()
    g.user = current_user

#after_request - A decorator to mark a function as running before the response is returned.
@app.after_request
def after_request(response):
    """Close the database connection after each request"""
    g.db.close()

    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    
    return response


@app.route('/ticket_check',methods=('GET', 'POST'))
def ticket_check():
    form = forms.TicketCheck()
    if form.validate_on_submit():
        return redirect(url_for('ticket', ticket_no = form.ticket_no.data ))
            
    return render_template('ticket_check.html', form=form)

@app.route('/ticket/<ticket_no>')
def ticket(ticket_no):
    booking = models.Booking.get(models.Booking.serial_no == ticket_no)
    trip_info = models.Trip
    return render_template('ticket.html', book = booking, trip = trip_info)
    

@app.route('/',methods=('GET', 'POST'))
def index():
   
    form = forms.BusSearch()
    if form.validate_on_submit():

        return redirect(url_for('search',from_loc=form.location_from.data, 
                                        to_loc=form.location_to.data, 
                                        date=form.date_dep.data,
                                        date_active = form.date_active.data))
       

    return render_template('index.html', form=form)
    



@app.route('/login', methods=('GET', 'POST'))
def login():
    form = forms.LoginForm()
    if form.validate_on_submit():
        try:
            user = models.User.get(models.User.email == form.email.data)
        except models.DoesNotExist:
            flash("Your email or password doesn't match!", "error")
        else:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                flash("You've been logged in!", "success")
                return redirect(url_for('admin'))
            else:
                flash("Your email or password doesn't match!", "error")

    
    return render_template('login.html', form=form)

@app.route('/create_trip',methods=('GET', 'POST'))
@login_required
def create_trip():
    form = forms.TripForm()
    if form.validate_on_submit():
        
        models.Trip.create_trip(    #If the data is validated, we'll add it to the database
            from_loc = form.location_from.data,
            to_loc = form.location_to.data,
            date_dep = form.date_dep.data, 
            time_dep = form.time_dep.data,
            bus_no = form.bus_no.data,
            price = form.price.data,
            active = False
        )
        models.SeatConfig.make_seatconfig(models.Trip.select().order_by(models.Trip.id.desc()).get())
        flash("Trip Created!", "success")
        return redirect(url_for('admin_trips'))   #With the user added, we'll redirect to the index page
    return render_template('create_trip.html', form=form)

@app.route('/search/<from_loc>/<to_loc>/<date>/<date_active>')
def search(from_loc,to_loc,date,date_active):

    trips_loc = models.Trip.select().where(models.Trip.from_loc == from_loc).where(models.Trip.to_loc == to_loc)
    trips = trips_loc
    
    if (date_active == 'False'):
        trips = trips_loc.where(models.Trip.date_dep == date)  

    if trips.count() == 0:
        if (date_active == 'False'):
        
            flash("No Bus found!, activate Non-specific Date", "error")
        else:
            flash("No such bus Available at the moment!", "error")
        return redirect(url_for('index'))     

    return render_template('search_list.html', trips=trips, froml=from_loc, tol=to_loc)



@app.route('/set_seat/<trip_id>',methods=('GET', 'POST'))
def set_seat(trip_id):
    MODED_CHOICES = [(c, c) for c in models.SeatConfig.get_taken_seats(trip_id)]
    form = forms.SeatForm1()
    form.example.choices = MODED_CHOICES
    seat_string = ''  
    if form.is_submitted():
        seat_list = form.example.data
        count = len(seat_list)
        if seat_list == []:
            flash("Please Choose a seat!", "error")
        else:
            for x in seat_list:
                seat_string = seat_string + '-'+x

            seat_string = str(seat_string)
            # 
            # flash(models.SeatConfig.get_taken_seats(trip_id))

            return redirect(url_for('book',trip_id=trip_id, seat_string=seat_string, count= count))
            
            
            
    return render_template('set_seat.html', form =form)
  
@app.route('/book/<trip_id>/<seat_string>/<count>',methods=('GET', 'POST'))
def book(trip_id,seat_string,count):
    # form = forms.BookingForm()

    seat_string=str(seat_string)
    seat_list = seat_string.split('-')
    seat_list = seat_list[1:]

    seat_count = count
    created_trips=[]
   
    form_dic = {}
    for x in seat_list:
        globals()['form%s' % int(x)] = forms.BookingForm()
        form_dic[x]=globals()['form%s' % int(x)]

    if request.method == 'POST':
        form_name = request.form['form-name']
        for x in seat_list:
            if form_name == ('S'+x) and form_dic[x].validate_on_submit():

                # if models.Booking.select().where(models.Booking.trip_id == trip_id).where(models.Booking.seat_no == int(x)).exists():
                #     flash("Seat already Booked")
                # #     flash("Seat already Booked")
                    
                models.Booking.make_booking(    #If the data is validated, we'll add it to the database
                                    trip = trip_id,
                                    
                                    firstname = form_dic[x].firstname.data,
                                    secondname = form_dic[x].secondname.data,
                                    id_passpt = form_dic[x].id_passpt.data,
                                    nationality = form_dic[x].nationality.data,
                                    phone_num = form_dic[x].phone_num.data,
                                    email = form_dic[x].email.data,
                                    seat_no = int(x),
                                    paid_for = False)
                seat_no = int(x)
                models.SeatConfig.update_seat(trip_id, seat_no)
              
                
                seat_list.remove(x)
                seat_string = ''
                for i in seat_list:
                    seat_string = seat_string + '-'+i

                seat_string = str(seat_string)
                if seat_list == []:
                    # flash("ALL SEATES BOOKED")
                    serial_string = ''
                    for i in models.Booking.select().order_by(models.Booking.id.desc()).limit(seat_count):
                        serial_string = serial_string + '-'+i.serial_no
                    flash('Details For seat [%s]: Submited' % int(x) , 'success')
                    return redirect(url_for('payment', serial_string = serial_string))
                else:
                    flash('Details For seat [%s]: Submited' % int(x) , 'success')
                    return redirect(url_for('book',trip_id=trip_id, seat_string=seat_string, count=count))

        
          
    return render_template('book.html', form_dic =form_dic,seat_list=seat_list)
                   # if x == seat_list[-1]:
        # if len(seat_list) == 3:
        #     happne = False

@app.route('/payment/<serial_string>')
def payment(serial_string):
    serial_string=str(serial_string)
    serial_list = serial_string.split('-')
    serial_list = serial_list[1:]

    model_dic = {}
    for x in serial_list:
        globals()['model%s' % int(x)] = models.Booking.get(models.Booking.serial_no == int(x))
        model_dic[x]=globals()['model%s' % int(x)]

    bookings = models.Booking
    trip_info = models.Trip
    # xx = trip_info.get(trip_info.id == model_dic['67650'].trip_id)
    # for x in xx:
    #     flash(x.price)
    # flash(xx.price)
    return render_template('payment.html',trip = trip_info, serials = serial_string,serial_list=serial_list, model_dic = model_dic)

 
@app.route('/admin')
@login_required
def admin():
    
    return render_template('dashboard.html')

@app.route('/admin_bookings')
@login_required
def admin_bookings():
    bookings = models.Booking
    trips = models.Trip
    busx = []
    for trip in models.Trip.select():
        busx.append(trip.bus_no)
    buses = list(set(busx))
    return render_template('admin_bookings.html', bookings=bookings, trips=trips, buses=buses)

@app.route('/simulate_pay/<row_id>')
@login_required
def simulate_pay(row_id):
    
    models.Booking.update_payment(row_id)
    bookings = models.Booking.select()
    return render_template('admin_bookings.html', bookings=bookings)

@app.route('/admin_trips_delete/<trip_id>')
@login_required
def admin_trips_delete(trip_id):
    models.Trip.delete().where(models.Trip.id == trip_id).execute()
    models.Booking.delete().where(models.Booking.trip  == trip_id).execute()
    models.SeatConfig.delete().where(models.SeatConfig.trip  == trip_id).execute()
    return redirect(url_for('admin_trips'))

@app.route('/admin_trips')
@login_required
def admin_trips():
    trips = models.Trip.select()
   
    return render_template('admin_trips.html', trips = trips)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("You've logged out!", "success")
    return redirect(url_for('index'))

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'),404




if __name__ == '__main__':

    #Creates our database
    models.initialize()
    try:
        #creates a superuser account for us if it doesn't already exist
        models.User.create_user(
         
            email='admin@gmail.com',
            password='admin')
           

    except ValueError:
        pass
    import random, threading, webbrowser

    port = 5000 + random.randint(0, 999)
    url = "http://127.0.0.1:{0}/".format(port)

    threading.Timer(1.25, lambda: webbrowser.open(url) ).start()

    app.run(port=port, debug=True)
    

